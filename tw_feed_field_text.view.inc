<?php
/**
 * @file
 * Views handler.
 */

/**
 * A handler to provide proper displays for dates.
 */
class tw_feed_views_handler_field_text extends views_handler_field {

  /**
   * Define option_definition function.
   */
  function option_definition() {
    $options = parent::option_definition();
    $options['filter_url'] = array('default' => 1);

    return $options;
  }

  /**
   * Define options_form function.
   */
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    $options = array(
      '0' => t('No'),
      '1' => t('Yes'),
    );
    $form['filter_url'] = array(
      '#title' => t('Filter URL'),
      '#description' => t('Converts text into hyperlinks automatically.'),
      '#type' => 'select',
      '#options' => $options,
      '#default_value' => $this->options['filter_url'],
    );
  }

  /**
   * Define render function.
   */
  function render($values) {
    $message = $this->get_value($values);
    $filter = new stdClass();
    $filter->settings = 500;

    if ($message && $this->options['filter_url']) {
      // Fix url links.
      $output = _filter_url($message, $filter);

      // Replace @<user name>.
      $pattern = '/\@([a-zA-Z\_]+)/';
      $replace = '<a href="http://twitter.com/' . strtolower('\1') . '">@\1</a>';
      $output = preg_replace($pattern, $replace, $output);

      // Replace #<hash tag>.
      $output = preg_replace('/(^|\s)#(\w*[a-zA-Z_]+\w*)/', '\1<a href="http://twitter.com/search?q=%23\2">#\2</a>', $output);
    }
    else {
      $output = '';
    }

    return $output;
  }
}
